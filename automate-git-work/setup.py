from setuptools import setup

setup(
   name='automate-git-work',
   version='1.0',
   description='A useful module to automate git action like check-in/commit a file , creating tags for commit etc.',
   author='Maruthappan Thaiagarajan',
   author_email='maruthu.thiagu@gmail.com',
   packages=['automate-git-work'],  #same as name
   #install_requires=['bar', 'greek'], #external packages as dependencies
)