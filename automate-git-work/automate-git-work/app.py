import logging.config
import requests
import UserAuth as auth
import jproperties as prop
import json

# loading logging properties from file
logging.config.fileConfig('logging.conf')

# create logger
logger = logging.getLogger('app')

logger.info("App auto-git has started")

logger.info("Loading application properties..")
properties = prop.Properties()
with open("app.properties", "rb") as f:
    properties.load(f)


def post_to_bitbucket(url, data, auth, files):
    logger.info("Posting file to bit bucket")
    response = requests.post(url,data=data,auth=auth,files=files)

    if 201 == response.status_code:
        logger.info("POST request is success")
    else:
        logger.error("POST request is not success." + response.text)

    return response


def get_info_from_bitbucket(url, auth):
    logger.info("Getting commit info from bitbucket")
    response = requests.get(url, auth=auth)

    if 200 == response.status_code:
        logger.info("GET request is success")
    else:
        logger.error("GET request is not success." + response.text)

    return response


def create_tag_for_commit(url, headers, data, auth):
    logger.info("Creating tag for the commit")
    response = requests.post(url, headers=headers, data=data, auth=auth)

    if response.status_code == 201:
        logger.info('Tag successfully created')
    else:
        logger.error("Tag creation is not success." + response.text)

    return response


# Authorisation for accessing Bit bucket repository remotely
auth = auth.UserAuth(properties.get('username').data, properties.get('password').data)
auth_tuple = (auth.get_username(), auth.get_password())

# Bit bucket end point
url = 'https://api.bitbucket.org/2.0/repositories/maruthuthiagu/PyBasics/src'

# files dict to specify the files to post
files = {
    '/test/test1.txt': ('../Files/test1.txt', open('../Files/test1.txt', 'rb'))
}

# additional attributes like commit message
data = {'message': 'update test/test1'}

response = post_to_bitbucket(url, data, auth_tuple, files)

url = 'https://api.bitbucket.org/2.0/repositories/maruthuthiagu/PyBasics/commits/?path=/test/test1.txt'

response = get_info_from_bitbucket(url, auth_tuple)

# print(response.text)

dict_response = json.loads(response.text)
c = dict_response.get('values')[0]

if c.get('message') == data.get('message'):
    hash_for_commit = c.get('hash')

logger.info("Commit hash for the checked in file - " + hash_for_commit)

# Creating tag to the commit

logger.info("Creating tag for commit - " + hash_for_commit)
headers = {'Content-Type': 'application/json', }

data = "{\"name\":\"tag_181120_v2\",\"target\":{\"hash\": \"" + hash_for_commit + "\" }}"

url = 'https://api.bitbucket.org/2.0/repositories/maruthuthiagu/PyBasics/refs/tags'

headers = {
    'Content-Type': 'application/json',
}

response = create_tag_for_commit(url, {'Content-Type': 'application/json'}, data, auth_tuple)
